pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[derive(PartialEq, Clone, Copy, Debug)]
pub enum C {
    D,
    F
}

struct Money {
    amount: u32,
    currency: C
}

impl Money {
    fn new(amount: u32, currency: C) -> Money {
        Money{amount: amount, currency: currency}
    }

    fn times(&self, multiplier: u32) -> Money {
        Money{amount: self.amount * multiplier, currency: self.currency}
    }

    fn equals(&self, other: Self) -> bool {
        return self.amount == other.amount && self.currency == other.currency;
    }

    fn currency(&self) -> &C {
        return &self.currency;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_multiplication() {
        let five = Money::new(5, C::D);
        let product = five.times(2);
        assert!(Money::new(10, C::D).equals(product));
        let product2 = five.times(3);
        assert!(Money{amount: 15, currency:C::D}.equals(product2));

    }

    #[test]
    fn test_equality() {
        assert!(Money{amount: 5, currency: C::D}.equals(Money{amount: 5, currency: C::D}));
        assert!(
            ! Money{amount: 5, currency: C::D}.equals(Money{amount: 6, currency: C::D})
        );
    }

    #[test]
    fn test_franc_multiplication() {
        let five = Money::new(5, C::F);
        let product = five.times(2);
        assert!(Money::new(10, C::F).equals(product));
        let product2 = five.times(3);
        assert!(Money::new(15, C::F).equals(product2));

    }
    #[test]
    fn test_currency() {
        assert_eq!(&C::D, Money::new(1, C::D).currency());
        assert_eq!(&C::F, Money::new(1, C::F).currency())

    }

    #[test]
    fn test_currency_mismatch() {
        assert!(
            Money::new(1, C::D).currency()
                !=
                Money::new(1, C::F).currency()
        );
    }
}
